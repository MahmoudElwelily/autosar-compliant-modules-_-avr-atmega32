
#ifndef DEM_H
#define DEM_H

#include "Std_Types.h"

#define DEM_EVENT_STATUS_PASSED ((uint8)(0x00))
#define DEM_EVENT_STATUS_FAILED ((uint8)(0x01))

typedef u16 Dem_EventIdType;
typedef u8 Dem_EventStatusType;

#define DEM_START_SEC_CODE
#include "MemMap.h"

extern FUNC(void, DEM_CODE) Dem_ReportErrorStatus (Dem_EventIdType EventID, Dem_EventStatusType EventStatus);

#define DEM_STOP_SEC_CODE
#include "MemMap.h"

#endif /* DEM_H */
