#ifndef DET_H
#define DET_H

#include "Std_Types.h"

#define DET_START_SEC_CODE
#include "MemMap.h"

extern FUNC(Std_ReturnType, DET_CODE) Det_ReportError ( 
                         u16 u16ModuleId,
                         u8  u8InstanceId,
                         u8  u8ApiId,
                         u8  u8ErrorId
                       );

#define DET_STOP_SEC_CODE
#include "MemMap.h"

#endif /* DET_H */
