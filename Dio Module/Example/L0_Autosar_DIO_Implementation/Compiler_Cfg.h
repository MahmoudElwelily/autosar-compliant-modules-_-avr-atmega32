
#ifndef COMPILER_CFG_H
#define COMPILER_CFG_H

#define     COMPILER_CFG_MAJOR_VERSION          (1U)
#define     COMPILER_CFG_MINOR_VERSION          (0U)
#define     COMPILER_CFG_PATCH_VERSION          (0U)

#define ISR_FUNC(IsrFunction)  __interrupt void IsrFunction(void)

#define CONSTP2FUNC(rettype, memclass, ptrclass, fctname)   rettype memclass (* ptrclass const fctname)

#define REGSPACE

/* Compiler memory class keywords */
/* Set to either __near or __far  */

/* DET */
#define DET_CODE								/* __far */
#define DET_VAR_NOINIT							/* __far */
#define DET_VAR_POWER_ON_INIT					/* __far */
#define DET_VAR_FAST 							/* __far */
#define DET_VAR									/* __far */
#define DET_CONST								/* __far */
#define DET_APPL_DATA							/* __far */
#define DET_APPL_CONST							/* __far */
#define DET_APPL_CODE 							/* __far */
#define DET_CALLOUT_CODE 						/* __far */
#define DET_CFG_CONST 							/* __far */

/* DEM */
#define DEM_CODE								/* __far */
#define DEM_VAR_NOINIT							/* __far */
#define DEM_VAR_POWER_ON_INIT					/* __far */
#define DEM_VAR_FAST 							/* __far */
#define DEM_VAR									/* __far */
#define DEM_CONST								/* __far */
#define DEM_APPL_DATA							/* __far */
#define DEM_APPL_CONST							/* __far */
#define DEM_APPL_CODE 							/* __far */
#define DEM_CALLOUT_CODE 						/* __far */
#define DEM_CFG_CONST 							/* __far */

/* EEP */
#define EEP_CODE								/* __far */
#define EEP_VAR_NOINIT							/* __far */
#define EEP_VAR_POWER_ON_INIT					/* __far */
#define EEP_VAR_FAST 							/* __far */
#define EEP_VAR									/* __far */
#define EEP_CONST								/* __far */
#define EEP_APPL_DATA							/* __far */
#define EEP_APPL_CONST							/* __far */
#define EEP_APPL_CODE 							/* __far */
#define EEP_CALLOUT_CODE 						/* __far */
#define EEP_CFG_CONST 							/* __far */

/* PORT */
#define PORT_CODE								/* __far */
#define PORT_VAR_NOINIT							/* __far */
#define PORT_VAR_POWER_ON_INIT					/* __far */
#define PORT_VAR_FAST 							/* __far */
#define PORT_VAR								/* __far */
#define PORT_CONST								/* __far */
#define PORT_APPL_DATA							/* __far */
#define PORT_APPL_CONST							/* __far */
#define PORT_APPL_CODE 							/* __far */
#define PORT_CALLOUT_CODE 						/* __far */
#define PORT_CFG_CONST 							/* __far */

/* DIO */
#define DIO_CODE								/* __far */
#define DIO_VAR_NOINIT							/* __far */
#define DIO_VAR_POWER_ON_INIT					/* __far */
#define DIO_VAR_FAST 							/* __far */
#define DIO_VAR									/* __far */
#define DIO_CONST								/* __far */
#define DIO_APPL_DATA							/* __far */
#define DIO_APPL_CONST							/* __far */
#define DIO_APPL_CODE 							/* __far */
#define DIO_CALLOUT_CODE 						/* __far */
#define DIO_CFG_CONST 							/* __far */

/* GPT */
#define GPT_CODE								/* __far */
#define GPT_VAR_NOINIT							/* __far */
#define GPT_VAR_POWER_ON_INIT					/* __far */
#define GPT_VAR_FAST 							/* __far */
#define GPT_VAR									/* __far */
#define GPT_CONST								/* __far */
#define GPT_APPL_DATA							/* __far */
#define GPT_APPL_CONST							/* __far */
#define GPT_APPL_CODE 							/* __far */
#define GPT_CALLOUT_CODE 						/* __far */
#define GPT_CFG_CONST 							/* __far */

/* SPI */
#define SPI_CODE								/* __far */
#define SPI_VAR_NOINIT							/* __far */
#define SPI_VAR_POWER_ON_INIT					/* __far */
#define SPI_VAR_FAST 							/* __far */
#define SPI_VAR									/* __far */
#define SPI_CONST								/* __far */
#define SPI_APPL_DATA							/* __far */
#define SPI_APPL_CONST							/* __far */
#define SPI_APPL_CODE 							/* __far */
#define SPI_CALLOUT_CODE 						/* __far */
#define SPI_CFG_CONST 							/* __far */

/* PWM */
#define PWM_CODE								/* __far */
#define PWM_VAR_NOINIT							/* __far */
#define PWM_VAR_POWER_ON_INIT					/* __far */
#define PWM_VAR_FAST 							/* __far */
#define PWM_VAR									/* __far */
#define PWM_CONST								/* __far */
#define PWM_APPL_DATA							/* __far */
#define PWM_APPL_CONST							/* __far */
#define PWM_APPL_CODE 							/* __far */
#define PWM_CALLOUT_CODE 						/* __far */
#define PWM_CFG_CONST 							/* __far */

/* ADC */
#define ADC_CODE								/* __far */
#define ADC_VAR_NOINIT							/* __far */
#define ADC_VAR_POWER_ON_INIT					/* __far */
#define ADC_VAR_FAST 							/* __far */
#define ADC_VAR									/* __far */
#define ADC_CONST								/* __far */
#define ADC_APPL_DATA							/* __far */
#define ADC_APPL_CONST							/* __far */
#define ADC_APPL_CODE 							/* __far */
#define ADC_CALLOUT_CODE 						/* __far */
#define ADC_CFG_CONST 							/* __far */

/* WDG */
#define WDG_CODE								/* __far */
#define WDG_VAR_NOINIT							/* __far */
#define WDG_VAR_POWER_ON_INIT					/* __far */
#define WDG_VAR_FAST 							/* __far */
#define WDG_VAR									/* __far */
#define WDG_CONST								/* __far */
#define WDG_APPL_DATA							/* __far */
#define WDG_APPL_CONST							/* __far */
#define WDG_APPL_CODE 							/* __far */
#define WDG_CALLOUT_CODE 						/* __far */
#define WDG_CFG_CONST 							/* __far */

/* ICU */
#define ICU_CODE								/* __far */
#define ICU_VAR_NOINIT							/* __far */
#define ICU_VAR_POWER_ON_INIT					/* __far */
#define ICU_VAR_FAST 							/* __far */
#define ICU_VAR									/* __far */
#define ICU_CONST								/* __far */
#define ICU_APPL_DATA							/* __far */
#define ICU_APPL_CONST							/* __far */
#define ICU_APPL_CODE 							/* __far */
#define ICU_CALLOUT_CODE 						/* __far */
#define ICU_CFG_CONST 							/* __far */

#endif /* COMPILER_CFG_H */
