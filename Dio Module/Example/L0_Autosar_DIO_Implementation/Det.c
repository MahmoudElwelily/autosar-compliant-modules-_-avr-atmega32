

#include "Std_Types.h"
#include "Compiler.h"

#include "Det.h"

#define DET_START_SEC_CODE
#include "MemMap.h"

FUNC(Std_ReturnType, DET_CODE) Det_ReportError (u16 u16ModuleId,
                                                u8 u8InstanceId,
                                                u8 u8ApiId,
                                                u8 u8ErrorId )
{
    return E_OK;
}

#define DET_STOP_SEC_CODE
#include "MemMap.h"
