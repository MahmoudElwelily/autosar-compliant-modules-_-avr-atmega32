
#ifndef COMPILER_H
#define COMPILER_H

#include "Compiler_Cfg.h"

#define     COMPILER_VENDOR_ID                          (uint16)(0)
#define     COMPILER_MODULE_ID                          (uint16)(0)

#define     COMPILER_AR_RELEASE_MAJOR_VERSION           (4U)
#define     COMPILER_AR_RELEASE_MINOR_VERSION           (0U)
#define     COMPILER_AR_RELEASE_REVISION_VERSION        (3U)

#define     COMPILER_SW_MAJOR_VERSION                   (1U)
#define     COMPILER_SW_MINOR_VERSION                   (0U)
#define     COMPILER_SW_PATCH_VERSION					(0U)

#define     _CODEWARRIOR_C_S12X_

#define     AUTOMATIC

#define     TYPEDEF

#define     NULL_PTR                            		((void *)0)

#define     INLINE

#define     LOCAL_INLINE

#define     FUNC(rettype, memclass)						rettype memclass

#define     FUNC_P2CONST(rettype, ptrclass, memclass)   const rettype memclass * ptrclass

#define     FUNC_P2VAR(rettype, ptrclass, memclass)     rettype memclass * ptrclass

#define     P2VAR(ptrtype, memclass, ptrclass)			ptrtype memclass * ptrclass

#define     P2CONST(ptrtype, memclass, ptrclass)		const ptrtype memclass * ptrclass

#define     CONSTP2VAR(ptrtype, memclass, ptrclass)		ptrtype memclass * ptrclass const

#define     CONSTP2CONST(ptrtype, memclass, ptrclass)	const ptrtype memclass * ptrclass const

#define     P2FUNC(rettype, ptrclass, fctname)			rettype (*ptrclass fctname)

#define     CONST(consttype, memclass)					const consttype memclass

#define     VAR(vartype, memclass)						vartype memclass

#endif /* COMPILER_H */
