
#include "Std_Types.h"
#include "Compiler.h"

#include "Dem.h"

#define DEM_START_SEC_CODE
#include "MemMap.h"

FUNC(void, DEM_CODE) Dem_ReportErrorStatus (Dem_EventIdType EventID, Dem_EventStatusType EventStatus)
{
}

#define DEM_STOP_SEC_CODE
#include "MemMap.h"
