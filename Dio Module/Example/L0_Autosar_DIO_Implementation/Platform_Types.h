
#ifndef PLATFORM_TYPES_H
#define PLATFORM_TYPES_H

#define     PLATFORM_VENDOR_ID                   (uint16)(0)
#define     PLATFORM_MODULE_ID                   (uint16)(0)

#define     PLATFORM_AR_RELEASE_MAJOR_VERSION    (4U)
#define     PLATFORM_AR_RELEASE_MINOR_VERSION    (0U)
#define     PLATFORM_AR_RELEASE_REVISION_VERSION (3U)

#define     PLATFORM_SW_MAJOR_VERSION            (1U)
#define     PLATFORM_SW_MINOR_VERSION            (0U)
#define     PLATFORM_SW_PATCH_VERSION            (0U)

#define     CPU_TYPE_8                          8
#define     CPU_TYPE_16                         16
#define     CPU_TYPE_32                         32

#define     MSB_FIRST                           0
#define     LSB_FIRST                           1

#define     HIGH_BYTE_FIRST                     0
#define     LOW_BYTE_FIRST                      1

#ifndef TRUE
#define     TRUE                                1
#endif /* TRUE */

#ifndef FALSE
#define     FALSE                               0
#endif /* FALSE */

/* Definitions for S12X */
#define     CPU_TYPE            CPU_TYPE_16
#define     CPU_BIT_ORDER       LSB_FIRST
#define     CPU_BYTE_ORDER      HIGH_BYTE_FIRST

/* unsigned 8 bit type */
typedef unsigned char      u8   ;
/* signed 8 bit type */
typedef signed char        s8   ;
/* unsigned 16 bit type */
typedef unsigned short int u16  ;
/* signed 16 bit type */
typedef signed short int   s16  ;
/* unsigned 32 bit type */
typedef unsigned long int  u32  ;
/* signed 32 bit type */
typedef signed long int    s32  ;
/* floating type 32 bit */
typedef float              f32  ;
/* floating type 64 bit */
typedef double             f64  ;

#endif /* PLATFORM_TYPES_H */
