/********************************************************************/
/* Author  : Mahmoud Alaa Elwelily                                  */
/* Date    : 20 May 2019                                            */
/* Version : V01                                                    */
/********************************************************************/
/* Description                                                      */
/* ------------                                                     */
/* Program file for using DIO peripheral in AVR ATmega32            */
/* ModuleID:1                                                       */
/*                                                                  */
/********************************************************************/
#include "Std_Types.h"
#include "BIT_CALC.h"
#include "Platform_Types.h"
#include "Compiler.h"
#include "Det.h"
#include "Dio.h"
#include "Dio_Cfg.h"
#include "Dio_Port.h"
#include "Dio_Priv.h"

/*
 * Description: Service to initialize the module
 * Inputs: Pointer to post-build configuration data
 * Output: none
 */
void Dio_Init(const Dio_ConfigType* ConfigPtr)
{
	/*checking the error possibilities of the inputs*/
	if (ConfigPtr == NULL)
	{
		#if (DIO_DEV_ERROR_DETECT == DET_ENABLE)
		Det_ReportError (1,0,0x10,DIO_E_PARAM_CONFIG);
		#endif
	}
	else
	{

	}
	return;
}

#if (DIO_VERSION_INFO_API == ON)
/*
 * Description: Service to get the version information of this module
 * Inputs: Pointer to where to store the version information of this module
 * Output: none
 */
void Dio_GetVersionInfo(Std_VersionInfoType* VersionInfo)
{
	/*checking the error possibilities of the inputs*/
	if (VersionInfo == NULL)
	{
		#if (DIO_DEV_ERROR_DETECT == ON)
		Det_ReportError (1,0,0x12,DIO_E_PARAM_POINTER);
		#endif
	}
	else
	{
		VersionInfo -> moduleID = 1;
		VersionInfo -> sw_major_version = 1;
		VersionInfo -> sw_minor_version = 2;
		VersionInfo -> sw_patch_version = 2;
		VersionInfo -> vendorID = 1;
	}
	return;
}
#endif

/*********************************/
/* APIs for the single channel   */
/*********************************/
/*
 * Description: Service to set a level of a channel
 * Inputs: the channel (pin) number and the level (STD_HIGH / STD_LOW)
 * Output: none
 */
void Dio_WriteChannel(Dio_ChannelType ChannelId,Dio_LevelType Level)
{
	/*checking the error possibilities of the inputs*/
	if (ChannelId >= DIO_u8MAXPINNB)
	{
		#if (DIO_DEV_ERROR_DETECT == ON)
		Det_ReportError (1,0,0x01,DIO_E_PARAM_INVALID_CHANNEL_ID);
		#endif
	}
	else
	{
		switch (ChannelId)
		{
			case (STD_HIGH):   /* if the required pin level is high */
			{
				/* if the assigned channel is in PORT A */
				if(ChannelId < DIO_u8_PIN8)
				{
					/* Checking if the assigned channel is output */
					if (GET_BIT(DIO_PU_DDRA,ChannelId) == 1)
					{
						ASSIGN_BIT (DIO_PU_PORTA,ChannelId,STD_HIGH);
					}
				}
				/* if the assigned channel is in PORT B */
				else if((ChannelId >= DIO_u8_PIN8) && (ChannelId < DIO_u8_PIN16))
				{
					/* Checking if the assigned channel is output */
					if (GET_BIT(DIO_PU_DDRB,ChannelId-DIO_u8_PIN8) == 1)
					{
						ASSIGN_BIT (DIO_PU_PORTB,ChannelId-DIO_u8_PIN8,STD_HIGH);
					}
				}
				/* if the assigned channel is in PORT C */
				else if((ChannelId >= DIO_u8_PIN16) && (ChannelId < DIO_u8_PIN24))
				{
					/* Checking if the assigned channel is output */
					if (GET_BIT(DIO_PU_DDRC,ChannelId-DIO_u8_PIN16) == 1)
					{
					    ASSIGN_BIT (DIO_PU_PORTC,ChannelId-DIO_u8_PIN16,STD_HIGH);
					}
				}
				/* if the assigned channel is in PORT D */
				else
				{
					/* Checking if the assigned channel is output */
					if (GET_BIT(DIO_PU_DDRD,ChannelId-DIO_u8_PIN24) == 1)
					{
						ASSIGN_BIT (DIO_PU_PORTD,ChannelId-DIO_u8_PIN24,STD_HIGH);
					}
				}
				break;
			}
			case(STD_LOW):   /* if the required channel level is low */
			{
				/* if the assigned channel is in PORT A */
				if(ChannelId < DIO_u8_PIN8)
				{
					/* Checking if the assigned channel is output */
					if (GET_BIT(DIO_PU_DDRA,ChannelId) == 1)
					{
						ASSIGN_BIT (DIO_PU_PORTA,ChannelId,STD_LOW);
					}
				}
				/* if the assigned channel is in PORT B */
				else if((ChannelId >= DIO_u8_PIN8) && (ChannelId < DIO_u8_PIN16))
				{
					/* Checking if the assigned channel is output */
					if (GET_BIT(DIO_PU_DDRB,ChannelId-DIO_u8_PIN8) == 1)
					{
						ASSIGN_BIT (DIO_PU_PORTB,ChannelId-DIO_u8_PIN8,STD_LOW);
					}
				}
				/* if the assigned channel is in PORT C */
				else if((ChannelId >= DIO_u8_PIN16) && (ChannelId < DIO_u8_PIN24))
				{
					/* Checking if the assigned channel is output */
					if (GET_BIT(DIO_PU_DDRC,ChannelId-DIO_u8_PIN16) == 1)
					{
						ASSIGN_BIT (DIO_PU_PORTC,ChannelId-DIO_u8_PIN16,STD_LOW);
					}
				}
				/* if the assigned channel is in PORT D */
				else
				{
					/* Checking if the assigned channel is output */
					if (GET_BIT(DIO_PU_DDRD,ChannelId-DIO_u8_PIN24) == 1)
					{
						ASSIGN_BIT (DIO_PU_PORTD,ChannelId-DIO_u8_PIN24,STD_LOW);
					}
				}
				break;
			}
		}
	}
}

/*
 * Description: Returns the value of the specified DIO channel
 * Inputs: the channel (pin) number
 * output: Dio_LevelType (STD_HIGH / STD_LOW)
 */
Dio_LevelType Dio_ReadChannel (Dio_ChannelType ChannelId)
{
	Dio_LevelType value;
	/*checking the error possibilities of the inputs*/
	if ((ChannelId >= DIO_u8MAXPINNB))
	{
		#if (DIO_DEV_ERROR_DETECT == ON)
		Det_ReportError (1,0,0x00,DIO_E_PARAM_INVALID_CHANNEL_ID);
		#endif
	}
	else
	{
		/* if the assigned channel is in PORT A */
		if(ChannelId < DIO_u8_PIN8)
		{
			value = GET_BIT(DIO_PU_PINA,ChannelId);
		}
		/* if the assigned channel is in PORT B */
		else if((ChannelId >= DIO_u8_PIN8) && (ChannelId < DIO_u8_PIN16))
		{
			value = GET_BIT(DIO_PU_PINB,ChannelId-DIO_u8_PIN8);
		}
		/* if the assigned channel is in PORT C */
		else if((ChannelId >= DIO_u8_PIN16) && (ChannelId < DIO_u8_PIN24))
		{
			value = GET_BIT(DIO_PU_PINC,ChannelId-DIO_u8_PIN16);
		}
		/* if the assigned channel is in PORT D */
		else
		{
			value = GET_BIT(DIO_PU_PIND,ChannelId-DIO_u8_PIN24);
		}
	}
	return value;
}

#if (DIO_FLIP_CHANNEL_API == ON)
/*
 * Description: Service to flip (change from 1 to 0 or from 0 to 1) the level of a channel and return
   the level of the channel after flip
 * Inputs: the channel (pin) number
 * output: Dio_LevelType (STD_HIGH / STD_LOW)
 */
Dio_LevelType Dio_FlipChannel(Dio_ChannelType ChannelId)
{
	Dio_LevelType value;
	/*checking the error possibilities of the inputs*/
	if ((ChannelId >= DIO_u8MAXPINNB))
	{
		#if (DIO_DEV_ERROR_DETECT == ON)
		Det_ReportError (1,0,0x11,DIO_E_PARAM_INVALID_CHANNEL_ID);
		#endif
	}
	else
	{
		/* if the assigned channel is in PORT A */
		if(ChannelId < DIO_u8_PIN8)
		{
			/* Checking if the assigned channel is output */
			if (GET_BIT(DIO_PU_DDRA,ChannelId) == 1)
			{
				TOGGLE_BIT(DIO_PU_PORTA,ChannelId);
				value = GET_BIT(DIO_PU_PINA,ChannelId);
			}
		}
		/* if the assigned channel is in PORT B */
		else if((ChannelId >= DIO_u8_PIN8) && (ChannelId < DIO_u8_PIN16))
		{
			/* Checking if the assigned channel is output */
			if (GET_BIT(DIO_PU_DDRB,ChannelId-DIO_u8_PIN8) == 1)
			{
				TOGGLE_BIT (DIO_PU_PORTB,ChannelId-DIO_u8_PIN8);
				value = GET_BIT(DIO_PU_PINB,ChannelId-DIO_u8_PIN8);
			}
		}
		/* if the assigned channel is in PORT C */
		else if((ChannelId >= DIO_u8_PIN16) && (ChannelId < DIO_u8_PIN24))
		{
			/* Checking if the assigned channel is output */
			if (GET_BIT(DIO_PU_DDRC,ChannelId-DIO_u8_PIN16) == 1)
			{
				TOGGLE_BIT (DIO_PU_PORTC,ChannelId-DIO_u8_PIN16);
				value = GET_BIT(DIO_PU_PINC,ChannelId-DIO_u8_PIN16);
			}
		}
		/* if the assigned channel is in PORT D */
		else
		{
			/* Checking if the assigned channel is output */
			if (GET_BIT(DIO_PU_DDRD,ChannelId-DIO_u8_PIN24) == 1)
			{
				TOGGLE_BIT (DIO_PU_PORTD,ChannelId-DIO_u8_PIN24);
				value = GET_BIT(DIO_PU_PIND,ChannelId-DIO_u8_PIN24);
			}
		}
	}
	return value;
}
#endif

/*********************************/
/* APIs for a group              */
/*********************************/
/*
 * Description: Service to set a subset of the adjoining bits of a port to a specified level
 * Inputs: Pointer to ChannelGroup and the Value to be written
 * Output: none
 */
void Dio_WriteChannelGroup (const Dio_ChannelGroupType* ChannelGroupIdPtr,Dio_PortLevelType Level)
{
	/*checking the error possibilities of the inputs*/
	if (ChannelGroupIdPtr->port >= DIO_u8MAXPortNB)
	{
		#if (DIO_DEV_ERROR_DETECT == ON)
		Det_ReportError (1,0,0x01,DIO_E_PARAM_INVALID_PORT_ID);
		#endif
	}
	else
	{
		/* if the assigned port is PORT A */
		if(ChannelGroupIdPtr->port == DIO_u8_PORT_A)
		{
			DIO_PU_PORTA &= ~(ChannelGroupIdPtr ->mask);
			DIO_PU_PORTA |= (ChannelGroupIdPtr->mask & (Level<<ChannelGroupIdPtr->offset));
		}
		/* if the assigned port is PORT B */
		else if(ChannelGroupIdPtr->port == DIO_u8_PORT_B)
		{
			DIO_PU_PORTB &= ~(ChannelGroupIdPtr ->mask);
			DIO_PU_PORTB |= (ChannelGroupIdPtr->mask & (Level<<ChannelGroupIdPtr->offset));
		}
		/* if the assigned port is PORT C */
		else if(ChannelGroupIdPtr->port == DIO_u8_PORT_C)
		{
			DIO_PU_PORTC &= ~(ChannelGroupIdPtr ->mask);
			DIO_PU_PORTC |= (ChannelGroupIdPtr->mask & (Level<<ChannelGroupIdPtr->offset));
		}
		/* if the assigned port is PORT D */
		else if(ChannelGroupIdPtr->port == DIO_u8_PORT_D)
		{
			DIO_PU_PORTD &= ~(ChannelGroupIdPtr ->mask);
			DIO_PU_PORTD |= (ChannelGroupIdPtr->mask & (Level<<ChannelGroupIdPtr->offset));
		}
	}
	return;
}

/*
 * Description: This Service reads a subset of the adjoining bits of a port
 * Inputs: Pointer to ChannelGroup
 * Output: Dio_PortLevelType (Level of a subset of the adjoining bits of a port)
 */
Dio_PortLevelType Dio_ReadChannelGroup (const Dio_ChannelGroupType* ChannelGroupIdPtr)
{
   u8 value;
	/*checking the error possibilities of the inputs*/
	if (ChannelGroupIdPtr == NULL)
	{
		#if (DIO_DEV_ERROR_DETECT == ON)
		Det_ReportError (1,0,0x04,DIO_E_PARAM_INVALID_GROUP);
		#endif
	}
	else
	{
		/* if the assigned port is PORT A */
		if(ChannelGroupIdPtr->port == DIO_u8_PORT_A)
		{
			value = ((DIO_PU_PINA & ChannelGroupIdPtr ->mask) >> ChannelGroupIdPtr->offset);
		}
		/* if the assigned port is PORT B */
		else if(ChannelGroupIdPtr->port == DIO_u8_PORT_B)
		{
			value = ((DIO_PU_PINB & ChannelGroupIdPtr ->mask) >> ChannelGroupIdPtr->offset);
		}
		/* if the assigned port is PORT C */
		else if(ChannelGroupIdPtr->port == DIO_u8_PORT_C)
		{
			value = ((DIO_PU_PINC & ChannelGroupIdPtr ->mask) >> ChannelGroupIdPtr->offset);
		}
		/* if the assigned port is PORT D */
		else if(ChannelGroupIdPtr->port == DIO_u8_PORT_D)
		{
			value = ((DIO_PU_PIND & ChannelGroupIdPtr ->mask) >> ChannelGroupIdPtr->offset);
		}
	}
	return value;
}

/*********************************/
/* APIs for a port               */
/*********************************/
/*
 * Description: Service to set a value of the port
 * Inputs:  the port id and Value to be written on it
 * Output: none
 */
void Dio_WritePort(Dio_PortType PortId,Dio_PortLevelType Level)
{
	/*checking the error possibilities of the input*/
	if (PortId >= DIO_u8MAXPortNB)
	{
		#if (DIO_DEV_ERROR_DETECT == ON)
		Det_ReportError (1,0,0x03,DIO_E_PARAM_INVALID_GROUP);
		#endif
	}
	else
	{
		/* if the assigned port is PORT A */
		if(PortId == DIO_u8_PORT_A)
		{
			ASSIGN_8BITS (DIO_PU_PORTA,Level);
		}
		/* if the assigned port is PORT B */
		else if(PortId == DIO_u8_PORT_B)
		{
			ASSIGN_8BITS (DIO_PU_PORTB,Level);
		}
		/* if the assigned port is PORT C */
		else if(PortId == DIO_u8_PORT_C)
		{
			ASSIGN_8BITS (DIO_PU_PORTC,Level);
		}
		/* if the assigned port is PORT D */
		else if(PortId == DIO_u8_PORT_D)
		{
			ASSIGN_8BITS (DIO_PU_PORTC,Level);
		}
	}
}

/*
 * Description: Returns the level of all channels of that port
 * Inputs: the port id
 * output:  Dio_PortLevelType (Level of all channels of that port)
 */
Dio_PortLevelType Dio_ReadPort (Dio_PortType PortId)
{
	u8 value;
	/*checking the error possibilities of the input*/
	if (PortId >= DIO_u8MAXPortNB)
	{
		#if (DIO_DEV_ERROR_DETECT == ON)
		Det_ReportError (1,0,0x02,DIO_E_PARAM_INVALID_PORT_ID);
		#endif
	}
	else
	{
		/* if the assigned port is in PORT A */
		if(PortId == DIO_u8_PORT_A)
		{
			value = DIO_PU_PINA;
		}
		/* if the assigned port is in PORT B */
		else if(PortId == DIO_u8_PORT_B)
		{
			value = DIO_PU_PINB;
		}
		/* if the assigned port is in PORT C */
		else if(PortId == DIO_u8_PORT_C)
		{
			value = DIO_PU_PINC;
		}
		/* if the assigned port is in PORT D */
		else if(PortId == DIO_u8_PORT_D)
		{
			value = DIO_PU_PIND;
		}
	}
	return value;
}
