/********************************************************************/
/* Author  : Mahmoud Alaa Elwelily                                  */
/* Date    : 20 May 2019                                            */
/* Version : V01                                                    */
/********************************************************************/
/* Description                                                      */
/* ------------                                                     */
/* Header file for using DIO peripheral in AVR ATmega32             */
/* containing the instantiation (=definition) of the DIO            */
/* configuration structure includes Dio_Cfg.h and uses the          */
/* defined values for  initialization  of  structure elements       */
/* ModuleID:1                                                       */
/*                                                                  */
/********************************************************************/
#include "Std_Types.h"
#include "Dio.h"
#include "Dio_Cfg.h"

const Dio_ChannelGroupType DioConfigData[DIO_GROUPS_NUMBER] =
{
   {
      MOTOR_CTL_PORT,
      5,
      0x60,
   },
   {
      MUX_SEL_PORT,
      1,
      0x1E,
   }
};
