
#ifndef STD_TYPES_H
#define STD_TYPES_H

#include "Platform_Types.h"
#include "Compiler.h"

#define     STD_VENDOR_ID                       (uint16)(0)
#define     STD_MODULE_ID                       (uint16)(0)

#define     STD_AR_RELEASE_MAJOR_VERSION        (4U)
#define     STD_AR_RELEASE_MINOR_VERSION        (0U)
#define     STD_AR_RELEASE_REVISION_VERSION     (3U)

#define     STD_SW_MAJOR_VERSION                (1U)
#define     STD_SW_MINOR_VERSION                (0U)
#define     STD_SW_PATCH_VERSION                (0U)

#ifndef STATUSTYPEDEFINED
#define STATUSTYPEDEFINED
#define     E_OK                                0x00
typedef     unsigned char   StatusType; /* OSEK compliance */
#endif /* STATUSTYPEDEFINED */

#define     E_NOT_OK                            0x01

#define     STD_HIGH                            0x01
#define     STD_LOW                             0x00

#define     STD_ACTIVE                          0x01
#define     STD_IDLE                            0x00

#define     STD_ON                              0x01
#define     STD_OFF                             0x00

/* Macro for NULL */
#define NULL ((void*)0)

typedef     u8       Std_ReturnType;

typedef struct
{
   u16   vendorID;
   u16   moduleID;
   u8    sw_major_version;
   u8    sw_minor_version;
   u8    sw_patch_version;
} Std_VersionInfoType;

#endif /* STD_TYPES_H */
