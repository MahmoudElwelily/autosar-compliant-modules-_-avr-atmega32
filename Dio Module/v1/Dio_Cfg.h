/********************************************************************/
/* Author  : Mahmoud Alaa Elwelily                                  */
/* Date    : 20 May 2019                                            */
/* Version : V01                                                    */
/********************************************************************/
/* Description                                                      */
/* ------------                                                     */
/* Header file for using DIO peripheral in AVR ATmega32             */
/* ModuleID:1                                                       */
/*                                                                  */
/********************************************************************/

/* Preprocessor Guard */
#ifndef CONFIG_H_
#define CONFIG_H_
#include "Dio_Port.h"
/*
 * Macro to enable/disable the development  error  detection
 * Ranges: ON
 *         OFF
 */
#define DIO_DEV_ERROR_DETECT       OFF

/*
 * Macro to Add / remove the service Dio_ GetVersionInfo() from the code
 * Ranges: ON
 *         OFF
 */
#define DIO_VERSION_INFO_API       OFF

/*
 * Macro to Add / remove the service Dio_FlipChannel() from the code
 * Ranges: ON
 *         OFF
 */
#define DIO_FLIP_CHANNEL_API        OFF

/* A configuration to the number of GPIO groups*/
#define DIO_GROUPS_NUMBER          2

#define LED0                 DIO_u8_PIN0
#define LED1                 DIO_u8_PIN2
#define SWITCH               DIO_u8_PIN1
#define LCD                  DIO_u8_PORT_D
#define MOTOR_CTL_PORT       DIO_u8_PORT_A
#define MUX_SEL_PORT         DIO_u8_PORT_B
#define MOTOR_CTL_GRP_PTR    (&DioConfigData[0])
#define MUX_SEL_GRP_PTR      (&DioConfigData[1])

#endif /* CONFIG_H_ */
