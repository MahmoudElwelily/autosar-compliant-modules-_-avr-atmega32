/********************************************************************/
/* Author  : Mahmoud Alaa Elwelily                                  */
/* Date    : 20 May 2019                                            */
/* Version : V01                                                    */
/********************************************************************/
/* Description                                                      */
/* ------------                                                     */
/* Header file for using DIO peripheral in AVR ATmega32             */
/* ModuleID:1                                                       */
/*                                                                  */
/********************************************************************/
/* PreProcessor Guard */
#ifndef DIO_H_
#define DIO_H_
#include "Std_Types.h"
/*===================================================================*/
/*                     Data types Definitions                        */
/*===================================================================*/
/* Type for the definition of the Numeric ID of a DIO channel */
typedef u8 Dio_ChannelType;
/* Type for the definition of the Numeric ID of a DIO port */
typedef u8 Dio_PortType;

/* Type for the definition of the possible levels a DIO channel can have */
typedef u8 Dio_LevelType;

/* Macro to define the possible levels on each DIO channel in a certain port */
typedef u8 Dio_PortLevelType;

/* Type for the definition of a channel group, which consists of several adjoining
channels within a port */
typedef struct
{
	Dio_PortType port;
	u8 offset;
	u8 mask;
}Dio_ChannelGroupType;

/* Type for the definition of a configured channel */
typedef struct
{
  u8 fsdfs;
}Dio_ConfigType;

/*===================================================================*/
/*                          APIs                                     */
/*===================================================================*/

/*
 * Description: Service to initialize the module
 * Inputs: Pointer to post-build configuration data
 * Output: none
 */
extern void Dio_Init(const Dio_ConfigType* ConfigPtr);

/*
 * Description: Service to get the version information of this module
 * Inputs: Pointer to where to store the version information of this module
 * Output: none
 */
extern void Dio_GetVersionInfo(Std_VersionInfoType* VersionInfo);

/*********************************/
/* APIs for the single channel   */
/*********************************/

/*
 * Description: Service to set a level of a channel
 * Inputs: the channel (pin) number and the level (STD_HIGH / STD_LOW)
 * Output: none
 */
extern void Dio_WriteChannel(Dio_ChannelType ChannelId,Dio_LevelType Level);

/*
 * Description: Returns the value of the specified DIO channel
 * Inputs: the channel (pin) number
 * output: Dio_LevelType (STD_HIGH / STD_LOW)
 */
extern Dio_LevelType Dio_ReadChannel (Dio_ChannelType ChannelId);

/*
 * Description: Service to flip (change from 1 to 0 or from 0 to 1) the level of a channel and return
   the level of the channel after flip
 * Inputs: the channel (pin) number
 * output: Dio_LevelType (STD_HIGH / STD_LOW)
 */
extern Dio_LevelType Dio_FlipChannel(Dio_ChannelType ChannelId);

/*********************************/
/* APIs for a channel group      */
/*********************************/
/*
 * Description: Service to set a subset of the adjoining bits of a port to a specified level
 * Inputs: Pointer to ChannelGroup and the Value to be written
 * Output: none
 */
extern void Dio_WriteChannelGroup (const Dio_ChannelGroupType* ChannelGroupIdPtr,Dio_PortLevelType Level);

/*
 * Description: This Service reads a subset of the adjoining bits of a port
 * Inputs: Pointer to ChannelGroup
 * Output: Dio_PortLevelType (Level of a subset of the adjoining bits of a port)
 */
extern Dio_PortLevelType Dio_ReadChannelGroup (const Dio_ChannelGroupType* ChannelGroupIdPtr);


/*********************************/
/* APIs for a port               */
/*********************************/
/*
 * Description: Service to set a value of the port
 * Inputs:  the port id and Value to be written on it
 * Output: none
 */
extern void Dio_WritePort(Dio_PortType PortId,Dio_PortLevelType Level);

/*
 * Description: Returns the level of all channels of that port
 * Inputs: the port id
 * output:  Dio_PortLevelType (Level of all channels of that port)
 */
extern Dio_PortLevelType Dio_ReadPort (Dio_PortType PortId);

#endif /* DIO_H_ */
