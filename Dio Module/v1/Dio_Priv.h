/********************************************************************/
/* Author  : Mahmoud Alaa Elwelily                                  */
/* Date    : 20 May 2019                                            */
/* Version : V01                                                    */
/********************************************************************/
/* Description                                                      */
/* ------------                                                     */
/* Private header file for using DIO peripheral in AVR ATmega32     */
/* It is included by interface.c and it is completely hidden from   */
/* any file in HAL                                                  */
/* ModuleID:1                                                       */
/********************************************************************/
/* PreProcessor Guard */
#ifndef DIO_PRIV_H_
#define DIO_PRIV_H_

/*================================================================= */
/* Macros to define the states of the configuration parameters */
#define ON      0
#define OFF     1

/* Defining multiple Development errors */
#define DIO_E_PARAM_INVALID_CHANNEL_ID      0x0A
#define DIO_E_PARAM_CONFIG                  0x10
#define DIO_E_PARAM_INVALID_PORT_ID         0x14
#define DIO_E_PARAM_INVALID_GROUP           0x1F
#define DIO_E_PARAM_POINTER                 0x20

/*======================================================== */
/* Macro for maximum number of available DIO pins */
#define DIO_u8MAXPINNB (u8)32

/* Macro for maximum number of available ports */
#define DIO_u8MAXPortNB (u8)4

/* Macro for number of pins in a single port */
#define DIO_u8NUMBER_OF_PINS_IN_PORT (u8)8

/* AVR ATmega32 DIO registers */
#define DIO_PU_DDRA *((volatile u8*) 0x3A)
#define DIO_PU_DDRB *((volatile u8*) 0x37)
#define DIO_PU_DDRC *((volatile u8*) 0x34)
#define DIO_PU_DDRD *((volatile u8*) 0x31)

#define DIO_PU_PORTA *((volatile u8*) 0x3B)
#define DIO_PU_PORTB *((volatile u8*) 0x38)
#define DIO_PU_PORTC *((volatile u8*) 0x35)
#define DIO_PU_PORTD *((volatile u8*) 0x32)

#define DIO_PU_PINA *((volatile u8*) 0x39)
#define DIO_PU_PINB *((volatile u8*) 0x36)
#define DIO_PU_PINC *((volatile u8*) 0x33)
#define DIO_PU_PIND *((volatile u8*) 0x30)

#endif /* DIO_PRIV_H_ */
